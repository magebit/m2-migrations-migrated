<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Setup;

use Magebit\Migrations\Helper\Data;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Symfony\Component\Filesystem\Filesystem;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Data
     */
    private $helper;

    /**
     * InstallSchema constructor.
     *
     * @param Filesystem $filesystem Filesystem.
     * @param Data $helper Migration helper.
     */
    public function __construct(
        Filesystem $filesystem,
        Data $helper
    ) {
        $this->filesystem = $filesystem;
        $this->helper = $helper;
    }

    /**
     * Installs DB schema for a module.
     *
     * @param SchemaSetupInterface $setup Setup schema interface.
     * @param ModuleContextInterface $context Context interface.
     *
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'magebit_migrations'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('magebit_migrations')
        )->addColumn('id',
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Id'
        )->addColumn(
            'version',
            Table::TYPE_TEXT,
            12,
            ['nullable' => false],
            'Version'
        )->addColumn(
            'is_executed',
            Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false, 'default' => '0'],
            'Migration executed'
        )->addIndex(
            $installer->getIdxName(
                $installer->getTable('magebit_migrations'),
                ['version'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['version'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'Magebit Migrations'
        );

        $installer->getConnection()->createTable($table);

        $migrationDir = $this->helper->getMigrationPath();

        if (!$this->filesystem->exists($migrationDir)) {
            $this->filesystem->mkdir($migrationDir);
        }

        $installer->endSetup();
    }
}
