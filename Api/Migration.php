<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Api;

use Magebit\Migrations\Model\MigrationContext;

interface Migration
{
    /**
     * Processed when 'bin/magento migrate' is executed.
     *
     * @param MigrationContext $context Migration context.
     *
     * @return void
     */
    function onUpgrade(MigrationContext $context);
}