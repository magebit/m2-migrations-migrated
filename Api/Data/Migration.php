<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Api\Data;

interface Migration
{
    const VERSION  = 'version';
    const EXECUTED = 'is_executed';

    /**
     * Getter for migration version.
     *
     * @return string
     */
    function getVersion(): string;

    /**
     * Setter for version.
     *
     * @param string $version Version.
     *
     * @return \Magebit\Migrations\Model\Migration
     */
    function setVersion(string $version);

    /**
     * Check if migration has been executed.
     *
     * @return bool
     */
    function isExecuted(): bool;

    /**
     * Set if migration is executed.
     *
     * @param bool $executed If migration is executed.
     *
     * @return \Magebit\Migrations\Model\Migration
     */
    function setIsExecuted(bool $executed);
}