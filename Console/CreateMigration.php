<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Console;

use Magebit\Migrations\Helper\Data;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class CreateMigration extends Command
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Data
     */
    private $helper;

    /**
     * CreateMigration constructor.
     *
     * @param Filesystem $filesystem Filesystem.
     * @param Data $helper Migration helper.
     */
    public function __construct(
        Filesystem $filesystem,
        Data $helper
    ) {
        parent::__construct();

        $this->filesystem = $filesystem;
        $this->helper = $helper;
    }

    /**
     * Configure command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('migrate:new')
            ->setDescription('Create empty migration')
            ->addArgument('comment', InputArgument::OPTIONAL, 'What\'s migration about?');
    }

    /**
     * Create new migration stub.
     *
     * @param InputInterface $input Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $comment = $this->getComment($input);
        $template = file_get_contents(__DIR__ . '/../Template/NewMigration.stub');

        $fileName = time() . $comment . '.php';
        $fileName = $this->helper->getMigrationPath($fileName);

        if ($this->filesystem->exists($fileName)) {
            $output->writeln('<error>Migration with such name already exists!</error>');
            return;
        }

        file_put_contents($fileName, $template);

        $output->writeln('<info>Migration created!</info>');
    }

    /**
     * Get comment in correct format.
     *
     * @param InputInterface $input Input interface.
     *
     * @return string
     */
    protected function getComment(InputInterface $input): string
    {
        if (!($comment = $input->getArgument('comment'))) {
            return '';
        }

        $words = explode(' ', $comment);
        $words = array_map('strtolower', $words);

        return '_' . implode('_', $words);
    }
}
