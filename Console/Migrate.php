<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Console;

use Magebit\Migrations\Api\Migration as MigrationInterface;
use Magebit\Migrations\Helper\Data;
use Magebit\Migrations\Model\Migration;
use Magebit\Migrations\Model\MigrationContext;
use Magebit\Migrations\Model\ResourceModel\Migration as MigrationResource;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends Command
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var MigrationContext
     */
    private $migrationContext;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Migration
     */
    private $migration;

    /**
     * @var MigrationResource
     */
    private $migrationResource;

    /**
     * Migrate constructor.
     *
     * @param ModuleDataSetupInterface $setup Module setup interface.
     * @param MigrationContext $migrationContext Migration context.
     * @param Data $data Migration helper.
     * @param Migration $migration Migration model.
     * @param MigrationResource $migrationResource Migration resource model.
     *
     * @throws \LogicException Logical exception.
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        MigrationContext $migrationContext,
        Data $data,
        Migration $migration,
        MigrationResource $migrationResource
    ) {
        parent::__construct();

        $this->setup = $setup;
        $this->migrationContext = $migrationContext;
        $this->helper = $data;
        $this->migration = $migration;
        $this->migrationResource = $migrationResource;
    }

    /**
     * Configure command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('migrate')
            ->setDescription('Run migrations');
    }

    /**
     * Run migrations.
     *
     * @param InputInterface $input Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $installer = $this->setup;
        $installer->startSetup();

        $this->migrationContext->addOutput($output);
        $this->migrationContext->setSetup($installer);
        $this->migrationContext->setCommandComponent($this);

        $this->processMigrations($output);

        $installer->endSetup();
    }

    /**
     * Loop through all migrations and run ones that have not been executed, yet.
     *
     * @param OutputInterface $output Output interface.
     *
     * @return void
     * @throws LocalizedException Localized exception.
     */
    protected function processMigrations(OutputInterface $output)
    {
        $migrations = $this->helper->getMigrationList();

        try {
            foreach ($migrations as $fileInfo) {
                if (empty($migrationVersion = $this->helper->nameMatchesMigrationPattern($fileInfo))) {
                    continue;
                }

                $fileName = pathinfo($fileInfo->getFilename(), PATHINFO_FILENAME);
                $migration = $this->loadMigration($migrationVersion);

                if ($migration->isExecuted()) {
                    continue;
                }

                /** @var MigrationInterface $migrationClass */
                $migrationClass = require_once $fileInfo->getPathName();

                try {
                    $this->migrationContext->setAreaCode();

                    $migrationClass->onUpgrade($this->migrationContext);

                    $migration->setIsExecuted(true);
                    $this->migrationResource->save($migration);
                } catch (\Exception $e) {
                    throw new LocalizedException(
                        new Phrase($e->getMessage())
                    );
                }

                $output->writeln('<info>' . $fileName . ' successfully executed!</info>');
            }
        } catch (\Exception $e) {
            $output->writeln('<error>' . $fileName . ' failed!</error>');
            throw $e;
        }
    }

    /**
     * Load migration data by version.
     *
     * @param string $version Version.
     *
     * @return Migration
     */
    protected function loadMigration(string $version)
    {
        $migration = clone $this->migration;
        $this->migrationResource
            ->load($migration, $version, \Magebit\Migrations\Api\Data\Migration::VERSION);

        if (empty($migration->getVersion())) {
            $migration->setVersion($version);
        }

        return $migration;
    }
}
