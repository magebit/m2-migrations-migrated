<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Console;

use Magebit\Migrations\Api\Migration as MigrationInterface;
use Magebit\Migrations\Helper\Data;
use Magebit\Migrations\Model\Migration;
use Magebit\Migrations\Model\MigrationContext;
use Magebit\Migrations\Model\ResourceModel\Migration as MigrationResource;
use Magebit\Migrations\Model\ResourceModel\Migration\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\SplFileInfo;

class Revert extends Command
{
    /**
     * @var Migration
     */
    private $migration;

    /**
     * @var MigrationResource
     */
    private $migrationResource;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var MigrationContext
     */
    private $migrationContext;

    /**
     * @var Collection
     */
    private $collection;

    /**
     * Revert constructor.
     *
     * @param ModuleDataSetupInterface $setup Module setup interface.
     * @param MigrationContext $migrationContext Migration context.
     * @param Migration $migration Migration model.
     * @param MigrationResource $migrationResource Migration resource model.
     * @param Data $helper Migration helper.
     * @param Collection $collection Migration collection.
     *
     * @throws \LogicException Logical exception.
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        MigrationContext $migrationContext,
        Migration $migration,
        MigrationResource $migrationResource,
        Data $helper,
        Collection $collection
    ) {
        parent::__construct();

        $this->setup = $setup;
        $this->migrationContext = $migrationContext;
        $this->migration = $migration;
        $this->migrationResource = $migrationResource;
        $this->helper = $helper;
        $this->collection = $collection;
    }

    /**
     * Configure command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('revert')
            ->setDescription('Revert migrations one by one unless provided one')
            ->addArgument(
                'version',
                InputArgument::OPTIONAL,
                'Direct version of migration to revert'
            )
            ->addOption(
                'all',
                null,
                InputOption::VALUE_NONE,
                'Use this to revert all migrations'
            );
    }

    /**
     * Revert migrations.
     *
     * @param InputInterface $input Input interface.
     * @param OutputInterface $output Output interface.
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $installer = $this->setup;
        $installer->startSetup();

        $this->migrationContext->addOutput($output);
        $this->migrationContext->setSetup($installer);
        $this->migrationContext->setAreaCode();
        $this->migrationContext->setCommandComponent($this);

        list($version, $shouldRevertAll) = $this->getCommandData($input);

        if ($shouldRevertAll) {
            $this->revertAllMigrations($output);
        } else if ($version) {
            $this->revertMigration($version, $output);
        } else {
            $this->revertLastMigration($output);
        }

        $installer->endSetup();
    }

    /**
     * Get argument / option if provided.
     *
     * @param InputInterface $input Input interface.
     *
     * @return array
     */
    protected function getCommandData(InputInterface $input): array
    {
        return [
            $input->getArgument('version'),
            $input->getOption('all')
        ];
    }

    /**
     * Revert all migrations.
     *
     * @param OutputInterface $output Output interface.
     *
     * @return void
     */
    protected function revertAllMigrations(OutputInterface $output)
    {
        $collection = $this->collection
            ->addFieldToFilter(\Magebit\Migrations\Api\Data\Migration::EXECUTED, 1)
            ->setOrder('version');

        if (!$collection->getSize()) {
            $output->writeln('<error>All migrations already reverted!</error>');

            return;
        }

        foreach ($collection as $migration) {
            /** @var Migration $migration */
            $this->revertMigration($migration->getVersion(), $output);
        }
    }

    /**
     * Revert migration.
     *
     * @param string $version Version number.
     * @param OutputInterface $output Output interface.
     *
     * @return bool
     */
    protected function revertMigration(string $version, OutputInterface $output): bool
    {
        $migrations = $this->helper->getMigrationList();
        $file = null;

        foreach ($migrations as $fileInfo) {
            $fileSplit = explode('_', str_replace('.php', '', $fileInfo->getFileName()));

            if (reset($fileSplit) === $version) {
                $file = $fileInfo;
            }
        }

        if (!$file) {
            $output->writeln('<error>Migration with version "' . $version . '" could not be found!</error>');

            return false;
        }

        $migration = $this->loadMigration($version);
        $this->processRevertForMigration($migration, $file);

        $output->writeln('<info>' . $version . ' has been reverted!</info>');

        return true;
    }

    /**
     * Revert last migration.
     *
     * @param OutputInterface $output Output interface.
     *
     * @return bool
     */
    protected function revertLastMigration(OutputInterface $output): bool
    {
        $collection = $this->collection
            ->addFieldToFilter(\Magebit\Migrations\Api\Data\Migration::EXECUTED, 1)
            ->setOrder('version');

        if (!$collection->getSize()) {
            $output->writeln('<error>All migrations already reverted!</error>');

            return false;
        }

        /** @var Migration $migration */
        $migration = $collection->getFirstItem();

        return $this->revertMigration($migration->getVersion(), $output);
    }

    /**
     * Process revert and update for migration.
     *
     * @param Migration $migration Migration data.
     * @param SplFileInfo $fileInfo File data.
     *
     * @return void
     * @throws LocalizedException Localized exception.
     */
    protected function processRevertForMigration(Migration $migration, SplFileInfo $fileInfo)
    {
        /** @var MigrationInterface $migrationClass */
        $migrationClass = require_once $fileInfo->getPathName();

        try {
            if (method_exists($migrationClass, 'onDowngrade')) {
                $migrationClass->onDowngrade($this->migrationContext);
            }

            $migration->setIsExecuted(false);
            $this->migrationResource->save($migration);
        } catch (\Exception $e) {
            throw new LocalizedException(
                new Phrase($e->getMessage())
            );
        }
    }

    /**
     * Load migration by version.
     *
     * @param string $version Version.
     *
     * @return Migration
     */
    protected function loadMigration(string $version): Migration
    {
        $migration = clone $this->migration;
        $this->migrationResource->load(
            $migration, $version, \Magebit\Migrations\Api\Data\Migration::VERSION
        );

        return $migration;
    }
}