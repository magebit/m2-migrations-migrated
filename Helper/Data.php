<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class Data extends AbstractHelper
{
    const MIGRATION_LOCATION = '/migrations';

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * Data constructor.
     *
     * @param Context $context Helper context.
     * @param DirectoryList $directoryList Directory list.
     * @param Finder $finder Finder.
     */
    public function __construct(
        Context $context,
        DirectoryList $directoryList,
        Finder $finder
    ) {
        parent::__construct($context);
        $this->directoryList = $directoryList;
        $this->finder = $finder;
    }

    /**
     * Get migration path.
     *
     * @param null $path Path.
     *
     * @return string
     */
    public function getMigrationPath($path = null): string
    {
        return $this->directoryList
                ->getPath(DirectoryList::APP) . self::MIGRATION_LOCATION . ($path ? '/' . $path : '');
    }

    /**
     * Get migration list.
     *
     * @return Finder
     */
    public function getMigrationList(): Finder
    {
        $finder = $this->finder
            ->name('*.php')
            ->in($this->getMigrationPath())
            ->sortByName();

        return $finder;
    }

    /**
     * Match if file name consists of timestamp if so return it.
     *
     * @param SplFileInfo $fileInfo File info.
     *
     * @return string
     */
    public function nameMatchesMigrationPattern(SplFileInfo $fileInfo): string
    {
        if (preg_match('/(?<!\d)\d{10}(?!\d)/', $fileInfo->getFilename(), $version)) {
            return $version[0];
        }

        return '';
    }
}
