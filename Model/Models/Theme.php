<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;
use Magento\Theme\Model\ResourceModel\Theme as ThemeResourceModel;
use Magento\Theme\Model\Theme as ThemeModel;

class Theme extends MigrationAPI
{
    /**
     * @var ThemeModel
     */
    private $theme;

    /**
     * @var ThemeResourceModel
     */
    private $themeResource;

    /**
     * @var Config
     */
    private $config;

    /**
     * Theme constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param ThemeModel $theme
     * @param ThemeResourceModel $themeResource
     * @param Config $config
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ThemeModel $theme,
        ThemeResourceModel $themeResource,
        Config $config
    ) {
        parent::__construct($objectManager);

        $this->theme = $theme;
        $this->themeResource = $themeResource;
        $this->config = $config;
    }

    /**
     * Set active theme by code.
     *
     * @param string $code
     *
     * @return Theme
     */
    public function setActiveTheme(string $code)
    {
        $this->config->setConfig('design/theme/theme_id', $this->loadTheme($code)->getId());

        return $this;
    }

    /**
     * Get path to theme file.
     *
     * @param string $path
     *
     * @return string
     */
    public function themePath(string $path)
    {
        list($theme, $file) = explode('::', $path);
        $theme = str_replace('_', '/', $theme);

        $themeDirectory = $this->loadTheme($theme)->getFullPath();

        return $this->getDirectory()
                ->getPath(DirectoryList::APP) . '/design/' . $themeDirectory . '/media/' . $file;
    }

    /**
     * Load theme.
     *
     * @param string $code
     *
     * @return ThemeModel
     */
    protected function loadTheme(string $code): ThemeModel
    {
        $theme = clone $this->theme;
        $this->themeResource->load($theme, $code, 'code');

        return $theme;
    }
}
