<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Category\Attribute as CategoryAttribute;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as ProductAttribute;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Customer\Model\Attribute as CustomerAttribute;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Model\Entity\TypeFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as AttributeResource;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\Collection as GroupCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\Collection;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class Attribute extends MigrationAPI
{
    const PRODUCT = 'product';
    const CATEGORY = 'category';
    const CUSTOMER = 'customer';

    /**
     * @var array
     */
    private $entities = [
        self::PRODUCT => Product::ENTITY,
        self::CATEGORY => Category::ENTITY,
        self::CUSTOMER => Customer::ENTITY
    ];

    /**
     * Allowed types.
     *
     * @var array
     */
    private $allowedTypes = [self::PRODUCT, self::CATEGORY, self::CUSTOMER];

    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * @var AttributeResource
     */
    private $attributeResource;

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * @var TypeFactory
     */
    private $eavTypeFactory;

    /**
     * @var Collection
     */
    private $setCollection;

    /**
     * @var GroupCollection
     */
    private $groupCollection;

    /**
     * @var AttributeManagement
     */
    private $attributeManagement;

    /**
     * Attribute constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param ModuleDataSetupInterface $setup
     * @param EavSetupFactory $eavSetupFactory
     * @param TypeFactory $eavTypeFactory
     * @param CategorySetupFactory $categorySetupFactory
     * @param CustomerSetupFactory $customerSetupFactory
     * @param AttributeResource $attributeResource
     * @param Collection $setCollection
     * @param GroupCollection $groupCollection
     * @param AttributeManagement $attributeManagement
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ModuleDataSetupInterface $setup,
        EavSetupFactory $eavSetupFactory,
        TypeFactory $eavTypeFactory,
        CategorySetupFactory $categorySetupFactory,
        CustomerSetupFactory $customerSetupFactory,
        AttributeResource $attributeResource,
        Collection $setCollection,
        GroupCollection $groupCollection,
        AttributeManagement $attributeManagement
    ) {
        parent::__construct($objectManager);

        $this->setup = $setup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavTypeFactory = $eavTypeFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeResource = $attributeResource;
        $this->setCollection = $setCollection;
        $this->groupCollection = $groupCollection;
        $this->attributeManagement = $attributeManagement;
    }

    /**
     * Create attribute.
     *
     * @param string $type
     * @param \Magento\Eav\Model\Entity\Attribute|array $data
     *
     * @return Attribute
     */
    public function createAttribute(string $type, $data): Attribute
    {
        list($model, $type) = $this->getEntityModel($type);

        if (!$model) {
            return $this;
        }

        if ($data instanceof \Magento\Eav\Model\Entity\Attribute) {
            $attributeCode = $data->getAttributeCode();
            $data = $data->getData();
        } else {
            $attributeCode = $data['attribute_code'];
        }

        /** @var EavSetup $model */
        $model->removeAttribute($type, $attributeCode);
        $model->addAttribute($type, $attributeCode, $data);

        return $this;
    }

    /**
     * Edit attribute entity.
     *
     * @param string $type
     * @param string $attributeCode
     * @param \Magento\Eav\Model\Entity\Attribute|array $data
     *
     * @return Attribute
     * @throws \LogicException Logical exception.
     */
    public function editAttribute(string $type, string $attributeCode, $data): Attribute
    {
        $model = null;

        switch ($type) {
            case self::PRODUCT:
                $model = $this->emptyProductAttribute();
                break;

            case self::CATEGORY:
                $model = $this->emptyCategoryAttribute();
                break;

            case self::CUSTOMER:
                $model = $this->emptyCustomerAttribute();
                break;
        }

        if (!$model) {
            return $this;
        }

        $entityType = $this->eavTypeFactory->create()->loadByCode($this->entities[$type]);
        if ($this->attributeResource->loadByCode($model, $entityType->getId(), $attributeCode)) {
            if ($data instanceof \Magento\Eav\Model\Entity\Attribute) {
                $data = $data->getData();
            }

            foreach ($data as $key => $value) {
                $model->setData($key, $value);
            }

            $this->attributeResource->save($model);
        } else {
            throw new \LogicException("editAttribute($type, $attributeCode): there is no attribute to edit.");
        }

        return $this;
    }

    /**
     * Delete attribute.
     *
     * @param string $type
     * @param string $attributeCode
     *
     * @return EavSetup
     */
    public function deleteAttribute(string $type, string $attributeCode): EavSetup
    {
        list($model, $type) = $this->getEntityModel($type);

        /** @var EavSetup $model */
        return $model->removeAttribute($type, $attributeCode);
    }

    /**
     * Add attribute to set and/or group.
     *
     * @param string $type
     * @param string $attributeCode
     * @param string $attributeSet
     * @param string $attributeGroup
     * @param int $sortOrder
     *
     * @return Attribute
     */
    public function addToSet(
        string $type,
        string $attributeCode,
        string $attributeSet = null,
        string $attributeGroup = null,
        int $sortOrder = 1000
    ): Attribute {
        if (!in_array($type, $this->allowedTypes)) {
            return $this;
        }

        $model = null;

        switch ($type) {
            case self::PRODUCT:
                $model = $this->emptyProductAttribute();
                break;

            case self::CATEGORY:
                $model = $this->emptyCategoryAttribute();
                break;

            case self::CUSTOMER:
                $model = $this->emptyCustomerAttribute();
                break;
        }

        $entityType = $this->eavTypeFactory->create()->loadByCode($this->entities[$type]);
        $this->attributeResource->loadByCode($model, $entityType->getId(), $attributeCode);

        if (!$model->getId()) {
            return $this;
        }

        $setCollection = $this->setCollection
            ->addFieldToFilter('entity_type_id', $entityType->getId());

        if (!$attributeSet) {
            $setCollection->addFieldToFilter('attribute_set_id', $entityType->getDefaultAttributeSetId());
        } else {
            $setCollection->addFieldToFilter('attribute_set_name', $attributeSet);
        }

        /** @var \Magento\Eav\Model\Entity\Attribute\Set $attributeSet */
        $attributeSet = $setCollection->getFirstItem();
        $groupCollection = $this->groupCollection
            ->addFieldToFilter('attribute_set_id', ['eq' => $attributeSet->getId()]);

        if (!$attributeGroup) {
            $groupCollection->addFieldToFilter('attribute_group_id', $attributeSet->getDefaultGroupId());
        } else {
            $groupCollection->addFieldToFilter('attribute_group_name', $attributeGroup);
        }

        /** @var \Magento\Eav\Model\Entity\Attribute\Group $group */
        $group = $groupCollection->getFirstItem();

        $this->attributeManagement->assign(
            $this->entities[$type], $attributeSet->getId(), $group->getAttributeGroupId(), $attributeCode, $sortOrder
        );

        return $this;
    }

    /**
     * Get entity model and type.
     *
     * @param string $type
     *
     * @return array|null
     */
    protected function getEntityModel(string $type)
    {
        $type = strtolower($type);

        if (!in_array($type, $this->allowedTypes)) {
            return null;
        }

        switch ($type) {
            case self::PRODUCT:
                return [
                    $this->eavSetupFactory->create(['setup' => $this->setup]),
                    Product::ENTITY
                ];

            case self::CATEGORY:
                return [
                    $this->categorySetupFactory->create(['setup' => $this->setup]),
                    Category::ENTITY
                ];

            case self::CUSTOMER:
                return [
                    $this->customerSetupFactory->create(['setup' => $this->setup]),
                    Customer::ENTITY
                ];
        }

        return null;
    }

    /**
     * Get empty product attribute model.
     *
     * @return ProductAttribute
     */
    public function emptyProductAttribute(): ProductAttribute
    {
        return $this->getObjectManager()->create(ProductAttribute::class);
    }

    /**
     * Get empty category attribute.
     *
     * @return CategoryAttribute
     */
    public function emptyCategoryAttribute(): CategoryAttribute
    {
        return $this->getObjectManager()->create(CategoryAttribute::class);
    }

    /**
     * Get empty customer attribute.
     *
     * @return CustomerAttribute
     */
    public function emptyCustomerAttribute(): CustomerAttribute
    {
        return $this->getObjectManager()->create(CustomerAttribute::class);
    }
}
