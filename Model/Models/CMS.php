<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Cms\Model\Block;
use Magento\Cms\Model\Page;
use Magento\Cms\Model\ResourceModel\Block as BlockResource;
use Magento\Cms\Model\ResourceModel\Page as PageResource;
use Magento\Framework\ObjectManagerInterface;
use Magento\UrlRewrite\Model\UrlRewrite;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewrite as UrlRewriteResource;

class CMS extends MigrationAPI
{
    /**
     * @var Page
     */
    private $page;

    /**
     * @var PageResource
     */
    private $pageResource;

    /**
     * @var Block
     */
    private $block;

    /**
     * @var BlockResource
     */
    private $blockResource;

    /**
     * @var UrlRewrite
     */
    private $urlRewrite;

    /**
     * @var UrlRewriteResource
     */
    private $urlRewriteResource;

    /**
     * CMS constructor.
     *
     * @param ObjectManagerInterface $objectManager Object manger interface.
     * @param Page $page
     * @param PageResource $pageResource
     * @param Block $block
     * @param BlockResource $blockResource
     * @param UrlRewrite $urlRewrite
     * @param UrlRewriteResource $urlRewriteResource
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Page $page,
        PageResource $pageResource,
        Block $block,
        BlockResource $blockResource,
        UrlRewrite $urlRewrite,
        UrlRewriteResource $urlRewriteResource
    ) {
        parent::__construct($objectManager);

        $this->page = $page;
        $this->pageResource = $pageResource;
        $this->block = $block;
        $this->blockResource = $blockResource;
        $this->urlRewrite = $urlRewrite;
        $this->urlRewriteResource = $urlRewriteResource;
    }

    /**
     * Create or modify cms pages.
     *
     * @param string|\Closure $identifier Either identifier or anonymous function.
     * @param null $name Name.
     * @param null $content Content.
     * @param array $store Array with 2 elements. 1st is current store id of page, 2nd array of new store ids for page.
     *
     * @return mixed
     */
    public function page($identifier, $name = null, $content = null, array $store = [0, [0]])
    {
        $page = clone $this->page;

        if ($identifier instanceof \Closure) {
            return $identifier($page, $this->pageResource);
        }

        list($store, $storeIds) = $store;

        $page->setStoreId([$store]);
        $this->pageResource->load($page, $identifier, 'identifier');

        $page
            ->setStoreId($storeIds)
            ->setIdentifier($identifier)
            ->setTitle($name)
            ->setContent($content);

        $this->pageResource->save($page);

        return $this;
    }

    /**
     * Load page by value and field.
     *
     * @param string $value
     * @param string $field
     * @param array $store
     *
     * @return Page
     */
    public function getPage(string $value, $field = 'identifier', array $store = [0]): Page
    {
        $page = clone $this->page;
        $page->setStoreId($store);

        $this->pageResource->load($page, $value, $field);

        return $page;
    }

    /**
     * Delete page by identifier.
     *
     * @param string $identifier
     * @param array $store
     *
     * @return CMS
     */
    public function deletePage(string $identifier, array $store = [0]): CMS
    {
        $page = clone $this->page;
        $page->setStoreId($store);

        $this->pageResource->load($page, $identifier, 'identifier');
        $this->pageResource->delete($page);

        return $this;
    }

    public function getPageRevisions(Page $page)
    {
        /** TODO: Create revisions */
    }

    public function revertPageRevision(Page $page, string $hashTo = null)
    {
        /** TODO: Create revisions */
    }

    /**
     * Create or modify cms blocks.
     *
     * @param string|\Closure $identifier Either identifier or anonymous function.
     * @param null $name Name.
     * @param null $content Content.
     * @param array $store Array with 2 elements. 1st is current store id of block, 2nd array of new store ids for block.
     *
     * @return mixed
     */
    public function block($identifier, $name = null, $content = null, array $store = [0, [0]])
    {
        $block = clone $this->block;

        if ($identifier instanceof \Closure) {
            return $identifier($block, $this->blockResource);
        }

        list($store, $storeIds) = $store;

        $block->setStoreId([$store]);
        $this->blockResource->load($block, $identifier, 'identifier');

        $block
            ->setStoreId($storeIds)
            ->setIdentifier($identifier)
            ->setTitle($name)
            ->setContent($content);

        $this->blockResource->save($block);

        return $this;
    }

    /**
     * Load block by value and field.
     *
     * @param string $value
     * @param string $field
     * @param array $store
     *
     * @return Block
     */
    public function getBlock(string $value, $field = 'identifier', array $store = [0]): Block
    {
        $block = clone $this->block;
        $block->setStoreId($store);

        $this->blockResource->load($block, $value, $field);

        return $block;
    }

    /**
     * Delete block by identifier.
     *
     * @param string $identifier
     * @param array $store
     *
     * @return CMS
     */
    public function deleteBlock(string $identifier, array $store = [0]): CMS
    {
        $block = clone $this->block;
        $block->setStoreId($store);

        $this->blockResource->load($block, $identifier, 'identifier');
        $this->blockResource->delete($block);

        return $this;
    }

    public function getBlockRevisions(Block $block)
    {
        /** TODO: Create revisions */
    }

    public function revertBlockRevision($block, string $hashTo = null)
    {
        /** TODO: Create revisions */
    }


    /**
     * Save url rewrite table.
     *
     * @param Page $page
     * @param bool $force
     *
     * @return void
     */
    public function createUrlRewrite(Page $page, bool $force = false)
    {
        $urlRewrite = clone $this->urlRewrite;
        $urlRewrite->setStoreId($page->getStoreId());

        $this->urlRewriteResource->load($urlRewrite, $page->getIdentifier(), 'request_path');

        if ($force && $urlRewrite->getId()) {
            $this->urlRewriteResource->delete($urlRewrite);
            $urlRewrite = clone $this->urlRewrite;
        } else if ($urlRewrite->getId()) {
            return;
        }

        $urlRewrite->setData([
            'entity_id' => $page->getId(),
            'request_path' => $page->getIdentifier(),
            'target_path' => 'cms/page/view/page_id/'. $page->getId(),
            'entity_type' => 'cms-page',
            'stores' => $page->getStores(),
            'is_autogenerated' => 1
        ]);

        $this->urlRewriteResource->save($urlRewrite);
    }
}
