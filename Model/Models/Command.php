<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Model\Models;

use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;

class Command extends MigrationAPI
{
    /**
     * @var SymfonyCommand
     */
    private $command;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * Command constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param SymfonyCommand $command
     * @param OutputInterface $output
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        SymfonyCommand $command,
        OutputInterface $output
    ) {
        parent::__construct($objectManager);

        $this->command = $command;
        $this->output = $output;
    }

    /**
     * Run console command.
     *
     * @param string $command
     * @param array $arguments
     *
     * @return int
     */
    public function runCommand(string $command, array $arguments = [])
    {
        $command = $this->command->getApplication()->find($command);

        $arguments = array_merge([
            'command' => $command
        ], $arguments);

        return $command->run(new ArrayInput($arguments), $this->output);
    }
}
