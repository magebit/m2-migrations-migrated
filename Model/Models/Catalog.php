<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Category as CategoryResource;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\ObjectManagerInterface;

class Catalog extends MigrationAPI
{
    const BY_SKU = 'sku';
    const BY_ID  = 'id';

    /**
     * @var Product
     */
    private $product;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var Category
     */
    private $category;

    /**
     * @var CategoryResource
     */
    private $categoryResource;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    /**
     * @var Collection
     */
    private $productCollection;
    /**
     * @var CategoryCollection
     */
    private $categoryCollection;

    /**
     * Catalog constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param Product $product
     * @param ProductResource $productResource
     * @param Category $category
     * @param CategoryResource $categoryResource
     * @param ProductRepository $productRepository
     * @param CategoryRepository $categoryRepository
     * @param Collection $productCollection
     * @param CategoryCollection $categoryCollection
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Product $product,
        ProductResource $productResource,
        Category $category,
        CategoryResource $categoryResource,
        ProductRepository $productRepository,
        CategoryRepository $categoryRepository,
        Collection $productCollection,
        CategoryCollection $categoryCollection
    ) {
        parent::__construct($objectManager);

        $this->product = $product;
        $this->productResource = $productResource;
        $this->category = $category;
        $this->categoryResource = $categoryResource;
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productCollection = $productCollection;
        $this->categoryCollection = $categoryCollection;
    }

    /**
     * Process custom functionality for product.
     *
     * @param \Closure  $callback Callback.
     *
     * @return mixed
     */
    public function product(\Closure $callback)
    {
        return $callback((clone $this->product), $this->productResource);
    }

    /**
     * Load product or get clean product model.
     *
     * @param null $value
     * @param int|null $storeId
     * @param string $by
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface|Product|mixed|null
     */
    public function getProduct($value = null, int $storeId = null, $by = self::BY_SKU)
    {
        if ($value) {
            return $this->getProductObject($value, $storeId, $by);
        }

        return clone $this->product;
    }

    /**
     * Get product collection.
     *
     * @return Collection
     */
    public function getProductCollection(): Collection
    {
        return clone $this->productCollection;
    }

    /**
     * Save product instance(s).
     *
     * @param Product[]|Product $product
     *
     * @return void
     */
    public function saveProduct($product)
    {
        if (is_array($product)) {
            foreach ($product as $item) {
                $this->saveProduct($item);
            }
        } else {
            $this->productRepository->save($product);
        }
    }

    /**
     * Delete product.
     *
     * @param $value
     * @param string $by
     *
     * @return bool
     */
    public function deleteProduct($value, string $by = self::BY_SKU): bool
    {
        $by = strtolower($by);

        if ($value instanceof Product) {
            return $this->productRepository->delete($value);
        }

        $model = $this->getProductObject($value, null, $by);

        if (!$model) {
            return false;
        }

        return $this->productRepository->delete($model);
    }

    /**
     * Get product object.
     *
     * @param string $value
     * @param int $storeId
     * @param string $by
     *
     * @return \Magento\Catalog\Api\Data\ProductInterface|mixed|null
     */
    protected function getProductObject(string $value, int $storeId, string $by)
    {
        if ($by == self::BY_SKU) {
            return $this->productRepository->get($value, true, $storeId);
        } else if ($by == self::BY_ID) {
            return $this->productRepository->getById($value, true, $storeId);
        }

        return null;
    }

    /**
     * Process custom functionality for category.
     *
     * @param \Closure  $callback Callback.
     *
     * @return mixed
     */
    public function category(\Closure $callback)
    {
        return $callback((clone $this->category), $this->categoryResource);
    }

    /**
     * Load category or get clean model.
     *
     * @param null $id
     * @param null $storeId
     *
     * @return \Magento\Catalog\Api\Data\CategoryInterface|Category|mixed
     */
    public function getCategory($id = null, $storeId = null)
    {
        if ($id) {
            return $this->categoryRepository->get($id, $storeId);
        }

        return clone $this->category;
    }

    /**
     * Get category collection.
     *
     * @return CategoryCollection
     */
    public function getCategoryCollection(): CategoryCollection
    {
        return clone $this->categoryCollection;
    }

    /**
     * Save category model.
     *
     * @param Category[]|Category $category
     *
     * @return void
     */
    public function saveCategory($category)
    {
        if (is_array($category)) {
            foreach ($category as $item) {
                $this->saveCategory($item);
            }
        } else {
            $this->categoryRepository->save($category);
        }
    }

    /**
     * Delete category.
     *
     * @param int $id
     *
     * @return bool
     */
    public function deleteCategory(int $id): bool
    {
        return $this->categoryRepository->deleteByIdentifier($id);
    }
}