<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\ObjectManagerInterface;

abstract class MigrationAPI
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var DirectoryList
     */
    private $directoryList;

    /**
     * MigrationAPI constructor.
     *
     * @param ObjectManagerInterface $objectManager Object manager interface.
     */
    public function __construct(
        ObjectManagerInterface $objectManager
    ) {
        $this->directoryList = $objectManager->get(DirectoryList::class);
        $this->objectManager = $objectManager;
    }

    /**
     * Get object manager.
     *
     * @return ObjectManagerInterface
     */
    protected function getObjectManager(): ObjectManagerInterface
    {
        return $this->objectManager;
    }

    /**
     * Get directory instance.
     *
     * @return DirectoryList
     */
    protected function getDirectory(): DirectoryList
    {
        return $this->directoryList;
    }

    /**
     * Get media directory location.
     *
     * @return string
     */
    protected function getMedia(): string
    {
        return $this->getDirectory()->getPath(DirectoryList::MEDIA);
    }
}