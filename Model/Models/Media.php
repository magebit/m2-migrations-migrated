<?php

namespace Magebit\Migrations\Model\Models;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

class Media extends MigrationAPI
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Theme
     */
    private $theme;

    /**
     * Media constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param Filesystem $filesystem
     * @param Theme $theme
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Filesystem $filesystem,
        Theme $theme
    ) {
        parent::__construct($objectManager);

        $this->filesystem = $filesystem;
        $this->theme = $theme;
    }

    /**
     * Copy file from theme folder to root media folder.
     *
     * @param array $media
     *
     * @return Media
     * @throws LocalizedException
     */
    public function copyMedia(array $media): Media
    {
        if (empty($media)) {
            return $this;
        }

        foreach ($media as $from => $to) {
            $toPath = $this->getDirectory()->getPath(DirectoryList::MEDIA) . '/' . $to;
            $fromPath = $this->theme->themePath($from);

            if (!$this->filesystem->exists($fromPath)) {
                continue;
            }

            $directory = pathinfo($toPath, PATHINFO_DIRNAME);
            $this->filesystem->mkdir($directory);

            try {
                $this->filesystem->copy($fromPath, $toPath, true);
            } catch (Exception $e) {
                throw new LocalizedException(
                    __($e->getMessage())
                );
            }
        }

        return $this;
    }

    /**
     * Delete media.
     *
     * @param array $media
     *
     * @return Media
     */
    public function deleteMedia(array $media): Media
    {
        if (empty($media)) {
            return $this;
        }

        foreach ($media as $path) {
            $path = $this->getDirectory()->getPath(DirectoryList::MEDIA) . '/' . $path;

            if (!$this->filesystem->exists($path)) {
                continue;
            }

            $this->filesystem->remove($path);
        }

        return $this;
    }
}
