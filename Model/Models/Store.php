<?php

namespace Magebit\Migrations\Model\Models;

use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;

class Store extends MigrationAPI
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Store constructor.
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($objectManager);

        $this->storeManager = $storeManager;
    }

    /**
     * Get stores.
     *
     * @return \Magento\Store\Api\Data\StoreInterface[]
     */
    public function getStores()
    {
        return $this->storeManager->getStores();
    }

    /**
     * Get store ids.
     *
     * @return array
     */
    public function getStoreIds()
    {
        $storeIds = [];

        foreach ($this->getStores() as $store) {
            $storeIds[] = $store->getId();
        }

        return $storeIds;
    }

    public function store()
    {

    }

    public function storeView()
    {

    }
}
