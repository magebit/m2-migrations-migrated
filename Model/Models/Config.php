<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Model\Models;

use Magento\Config\Model\ResourceModel\Config as ConfigResource;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;

class Config extends MigrationAPI
{
    /**
     * @var ConfigResource
     */
    private $configResource;

    /**
     * Config constructor.
     * @param ObjectManagerInterface $objectManager Object manager interface.
     * @param ConfigResource $configResource
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ConfigResource $configResource
    ) {
        parent::__construct($objectManager);

        $this->configResource = $configResource;
    }

    /**
     * Set config value by its path.
     *
     * @param string $configPath Config path.
     * @param mixed $configValue Config value.
     * @param mixed $website Website.
     * @param mixed $store Store.
     *
     * @return Config
     *
     * @throws LocalizedException Localized exception.
     */
    public function setConfig(
        string $configPath, $configValue, $website = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store = 0
    ): Config {
        $count = explode('/', $configPath);

        if (count($count) < 3) {
            throw new LocalizedException(
                new Phrase('Incorrect config path')
            );
        }

        $this->configResource->saveConfig($configPath, $configValue, $website, $store);

        return $this;
    }

    /**
     * Delete config by its path.
     *
     * @param string $configPath
     * @param string $website
     * @param int $store
     *
     * @return Config
     *
     * @throws LocalizedException
     */
    public function deleteConfig(string $configPath, $website = ScopeConfigInterface::SCOPE_TYPE_DEFAULT, $store = 0): Config
    {
        $count = explode('/', $configPath);

        if (count($count) < 3) {
            throw new LocalizedException(
                new Phrase('Incorrect config path')
            );
        }

        $this->configResource->deleteConfig(rtrim($configPath, '/'), $website, $store);

        return $this;
    }
}
