<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Model;

use Magebit\Migrations\Helper\Data;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Phrase;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationContext
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var State
     */
    private $state;

    /**
     * @var ModuleDataSetupInterface
     */
    private $setup;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var Models\Config
     */
    private $config;

    /**
     * @var Models\CMS
     */
    private $cms;

    /**
     * @var Models\Theme
     */
    private $theme;

    /**
     * @var Models\Catalog
     */
    private $catalog;

    /**
     * @var Models\Store
     */
    private $store;

    /**
     * @var Models\Media
     */
    private $media;

    /**
     * @var Models\Attribute
     */
    private $attribute;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Command
     */
    protected $commandComponent;

    /**
     * @var Models\Command
     */
    protected $command;

    /**
     * @var Models\Connection
     */
    protected $connection;

    /**
     * MigrationContext constructor.
     *
     * @param ObjectManagerInterface $objectManager Object manager.
     * @param State $state State.
     * @param Models\Config $config Config.
     * @param Models\CMS $cms
     * @param Models\Theme $theme
     * @param Models\Catalog $catalog
     * @param Models\Store $store
     * @param Models\Media $media
     * @param Data $helper
     * @param Models\Connection $connection
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        State $state,
        Models\Config $config,
        Models\CMS $cms,
        Models\Theme $theme,
        Models\Catalog $catalog,
        Models\Store $store,
        Models\Media $media,
        Data $helper,
        Models\Connection $connection
    ) {
        $this->objectManager = $objectManager;
        $this->state = $state;
        $this->config = $config;
        $this->cms = $cms;
        $this->theme = $theme;
        $this->catalog = $catalog;
        $this->store = $store;
        $this->media = $media;
        $this->helper = $helper;
        $this->connection = $connection;
    }

    /**
     * @return ObjectManagerInterface
     */
    public function getObjectManager(): ObjectManagerInterface
    {
        return $this->objectManager;
    }

    /**
     * Setter for setup.
     *
     * @param ModuleDataSetupInterface $setup Module setup interface.
     *
     * @return void
     */
    public function setSetup(ModuleDataSetupInterface $setup)
    {
        $this->setup = $setup;
    }

    /**
     * Getter for setup.
     *
     * @return ModuleDataSetupInterface
     */
    public function getSetup(): ModuleDataSetupInterface
    {
        return $this->setup;
    }

    /**
     * Add output for migrations.
     *
     * @param OutputInterface $output Output interface.
     *
     * @return void
     */
    public function addOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * Getter for output.
     *
     * @return OutputInterface
     */
    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    /**
     * Set area code.
     *
     * @param string $code Area code.
     *
     * @return void
     */
    public function setAreaCode($code = Area::AREA_ADMINHTML)
    {
        try {
            $this->state->setAreaCode($code);
        } catch (LocalizedException $e) {}
    }

    /**
     * Getter for Config model.
     *
     * @return Models\Config
     */
    public function config(): Models\Config
    {
        return $this->config;
    }

    /**
     * @return Models\CMS
     */
    public function cms(): Models\CMS
    {
        return $this->cms;
    }


    /**
     * @return Models\Theme
     */
    public function theme(): Models\Theme
    {
        return $this->theme;
    }

    /**
     * @return Models\Catalog
     */
    public function catalog(): Models\Catalog
    {
        return $this->catalog;
    }

    /**
     * @return Models\Store
     */
    public function store(): Models\Store
    {
        return $this->store;
    }

    /**
     * @return Models\Media
     */
    public function media(): Models\Media
    {
        return $this->media;
    }

    /**
     * @return Models\Attribute
     */
    public function attribute(): Models\Attribute
    {
        if (!$this->attribute) {
            $this->attribute = $this->getObjectManager()->create(
                Models\Attribute::class,
                ['setup' => $this->getSetup()]
            );
        }

        return $this->attribute;
    }

    /**
     * Load template, and if necessary assign variables.
     *
     * @param string $name
     * @param array $variables
     *
     * @return string
     *
     * @throws LocalizedException
     */
    public function getTemplate(string $name, array $variables = []): string
    {
        $path = $this->helper->getMigrationPath('template/' . $name . '.phtml');

        if (!file_exists($path)) {
            throw new LocalizedException(
                new Phrase("{$path} can't find template!")
            );
        }

        ob_start();

        $block = new DataObject($variables);
        require $path;

        return ob_get_clean();
    }

    /**
     * Set command component.
     *
     * @param Command $command
     *
     * @return void
     */
    public function setCommandComponent(Command $command)
    {
        $this->commandComponent = $command;
    }

    /**
     * Get command component.
     *
     * @return Command
     */
    protected function getCommandComponent(): Command
    {
        return $this->commandComponent;
    }

    /**
     * @return Models\Command
     */
    public function command(): Models\Command
    {
        if (!$this->command) {
            $this->command = $this->getObjectManager()->create(
                Models\Command::class,
                [
                    'command' => $this->getCommandComponent(),
                    'output' => $this->getOutput()
                ]
            );
        }

        return $this->command;
    }

    /**
     * Get connection model.
     *
     * @return Models\Connection
     */
    public function connection(): Models\Connection
    {
        return $this->connection;
    }
}
