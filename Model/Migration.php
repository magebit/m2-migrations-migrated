<?php
/**
 * Magebit_Migrations
 *
 * @category     Magebit
 * @package      Magebit_Migrations
 * @author       Raimonds Vizulis
 * @copyright    Copyright (c) 2017 Magebit, Ltd.(http://www.magebit.com/)
 */

namespace Magebit\Migrations\Model;

use Magebit\Migrations\Api\Data\Migration as MigrationInterface;
use Magebit\Migrations\Model\ResourceModel\Migration as MigrationResource;
use Magento\Framework\Model\AbstractModel;

class Migration extends AbstractModel implements MigrationInterface
{
    /**
     * Initialize model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(MigrationResource::class);
    }

    /**
     * Getter for migration version.
     *
     * @return string
     */
    public function getVersion(): string
    {
        return $this->getData(MigrationInterface::VERSION) ?: '';
    }

    /**
     * Setter for version.
     *
     * @param string $version Version.
     *
     * @return \Magebit\Migrations\Model\Migration
     */
    function setVersion(string $version)
    {
        return $this->setData(MigrationInterface::VERSION, $version);
    }

    /**
     * Check if migration has been executed.
     *
     * @return bool
     */
    function isExecuted(): bool
    {
        return $this->getData(MigrationInterface::EXECUTED) ?: false;
    }

    /**
     * Set if migration is executed.
     *
     * @param bool $executed If migration is executed.
     *
     * @return \Magebit\Migrations\Model\Migration
     */
    function setIsExecuted(bool $executed)
    {
        return $this->setData(MigrationInterface::EXECUTED, $executed);
    }
}
